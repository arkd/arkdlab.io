module.exports = {
    title: 'Aaron Matthew DeWitt\'s Blog',
    description: 'Another Programmer\'s blog',
    dest: 'public',
    plugins: [
        {
            '@vuepress/pwa': {
                serviceWorker: true,
                updatePopup: true
            }
        },
    ],
    themeConfig: {
        nav: [
            {text: 'Home', link: '/'},
        ],
        sidebar: [
            {
                title: 'My Weekly Blog',
                children: [
                    ['/about/', 'About Me'],
                    ['/about/week-9-2-19', 'My Week 9/2/19'],
                    ['/about/week-9-9-19', 'My Week 9/9/19'],
                    ['/about/week-9-16-19', 'My Week 9/16/19'],
                    ['/about/week-9-23-19', 'My Week 9/23/19'],
                    ['/about/week-9-30-19', 'My Week 9/30/19'],
                    ['/about/week-10-7-19', 'My Week 10/7/19'],
                    ['/about/week-10-14-19', 'My Week 10/14/19'],
                    ['/about/week-10-20-19', 'My Week 10/20/19'],
                    ['/about/week-10-28-19', 'My Week 10/28/19'],
                    ['/about/week-11-4-19', 'My Week 11/4/19'],
                    ['/about/week-11-18-19', 'My Week 11/18/19'],
                    ['/about/week-12-2-19', 'My Week 12/2/19'],
                ]
            },
        ]
    }
};

