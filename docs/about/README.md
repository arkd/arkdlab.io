---
prev: ../
next: ./week-9-2-19
---
# CS373 Fall 2019: Aaron Matthew DeWitt
![Aaron's bust](/bust.png)
## Where did grow up? 
Las Vegas

## What high school did you go to? 
Bonanza HS

## What was your favorite extracurricular activity in high school? 
Japanese Club

## Why did you come to UT? 
I wanted to get out of Vegas, since UNLV doesn't hav emuch in the way of science/ computer science education.

## Why are you majoring in CS?
Originally I was trying to transfer out and go into chemistry, but after a few classes I found that I loved CS more. Not long after that I was running servers, making websites, etc. I fell in love.

## How much Javascript/Python/Web programming do you already know?
I am Proficient.

## How did you like the first lecture? 
The First Lecture was OK, I like having clear expectations from the beginning.

## How did you feel about the cold calling? 
No Problem.

