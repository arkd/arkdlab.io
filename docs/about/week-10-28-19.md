---
prev: ./week-10-20-19
next: ./week-11-4-19
---

# week of 10/28/19
![Aaron's bust](/bust.png)

#### What did you do this past week?
fairly relaxed week.

 **Monday** Normal class start day, we are learning about Tomasulo in advanced computer architecture. I spent most of the rest of the day working on SwE and implementing a garbage collector in c++. 
 
**Tuesday** My normal volunteering was cancelled this week. So I spent the day working on backend work for my job and also on my c++ project. Had life group as usual in the evening. We read over James 4. James 4 is an incredible chapter because it tells you how to solve conflict in your life; where the source is, as we often see the symptoms not the source; and how to resolve them in a multi-step process. I was able to apply the word heavily this week, too bad it didn't help me with my laziness. Afterward I went home, watched a tv series and ended up staying up till 6am.

**Wednesday** I slept in late and missed class in the morning because I stayed up too late Tuesday night. I went to Campus anyway and I spent some time on my c++ project followed by SwE work.

**Thursday** I was incredibly lazy Thursday and got little done, even for work.

**Friday** I basically took Friday off, I felt pretty bad by this point but man I was struck with the lazy this week.

**Saturday** Volunteered at Oak Springs elementary school, they had a event for the kids with lots of contests and prizes. I helped mostly with the basketball stuff. Overall, it was a lot of fun, those kids are great! 

#### What's in your way?
If you can't tell I currently am struggling with my passions. I want to be a great programmer and enjoy a productive life but I also want to play games, read manga and watch TV shows. I am not saying there isn't a balance here but it eludes me. Sometimes my discipline fails and disastrous weeks like this one occur.

#### What will you do next week?
I have to catch up hard with everything I was lazy about last week.

#### What was your experience of SQL? (this question will vary, week to week)
SQL is one of my favorite languages because of how universal and straight forward it is. Even noSQL databases will still often accept SQL queries. It is one of the few places where CS has advanced enough to stabilize. Not to say there won't be innovations but I feel confident that even if the underlying implementation changes, SQL will remain the database language.

#### What's your pick-of-the-week or tip-of-the-week?
One of the many things I 'wasted' my time on this week was investigating Structures of Arrays(SOA). the simple idea is that instead of and array of  struct { x,y,z }, being laid out in memory: xyzxyzxyzxyz..., it could be laid out: xxxx...yyyy...zzzz... This is incredibly powerful performance tool, especially since we have SIMD (Single Instruction Multiple Data), that can be used to parallelize work (usually in groups of 4 but depends) on a CPU. I am very interested to see a working implementation that could take in a struct and output the correct memory layout for any size array. Even better would be the same for AOSOA, I leave you to investigate that if your interested!
